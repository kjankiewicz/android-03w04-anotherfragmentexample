Tytuł: **Wykorzystanie specjalizowanych klas fragmentów**

Zaprezentowane zagadnienia:

* Wykorzystanie klasy *DialogFragment* - do okien dialogowych
* Wykorzystanie klasy *PreferenceFragment* - do obsługi ustawień aplikacji
* Wykorzystanie klasy *WebViewFragment* - do prezentacji fragmentów formatowanego kodu (HTML)

Najważniejsze pliki:

* *MainActivity.java* - główna aktywność aplikacji, obsługa WebViewFragment, wywoływanie aktywności MyFragmentDialog oraz okna dialogowego
* *MyFragmentDialog.java* - implementacja okna dialogowego 
* *MySettingsActivity.java - aktywność prezentująca wykorzystanie klasy *PreferenceFragment*