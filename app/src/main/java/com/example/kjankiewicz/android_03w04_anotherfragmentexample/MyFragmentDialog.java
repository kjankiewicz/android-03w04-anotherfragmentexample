package com.example.kjankiewicz.android_03w04_anotherfragmentexample;


import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class MyFragmentDialog extends DialogFragment {
    Button mButton;
    EditText mEditText1, mEditText2;
    onSubmitListener mListener;
    String text = "";

    interface onSubmitListener {
        void setOnSubmitListener(String arg);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragment_my_fragment_dialog);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        mButton = dialog.findViewById(R.id.button1);
        mEditText1 = dialog.findViewById(R.id.editText1);
        mEditText2 = dialog.findViewById(R.id.editText2);
        mEditText1.setText(text);
        mEditText2.setText(text);
        mButton.setOnClickListener(v -> {
            Integer result = Integer.parseInt(mEditText1.getText().toString())+
                    Integer.parseInt(mEditText2.getText().toString());
            mListener.setOnSubmitListener(result.toString());
            dismiss();
        });
        return dialog;
    }
}

