package com.example.kjankiewicz.android_03w04_anotherfragmentexample;

import com.example.kjankiewicz.android_03w04_anotherfragmentexample.MyFragmentDialog.onSubmitListener;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebViewFragment;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;


public class MyActivity extends Activity implements onSubmitListener {
    TextView mTextView;
    Button mButton1, mButton2, mButton3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        mTextView = findViewById(R.id.textView1);
        mButton1 = findViewById(R.id.button1);
        mButton1.setOnClickListener(v -> {
            MyFragmentDialog fragment1 = new MyFragmentDialog();
            fragment1.mListener = MyActivity.this;
            fragment1.text = mTextView.getText().toString();
            fragment1.show(getFragmentManager(), "");
        });

        mButton2 = findViewById(R.id.button2);
        mButton2.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(MyActivity.this, MySettingsActivity.class);
            startActivityForResult(intent, 0);

        });

        mButton3 = findViewById(R.id.button3);
        mButton3.setOnClickListener(v -> {
            WebViewFragment webViewFragment = new WebViewFragment();

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.web_view_content, webViewFragment);
            transaction.commit();
            getFragmentManager().executePendingTransactions();
            if (webViewFragment.getWebView()==null)
                Log.d("webviewfragment", "is null");

            String summary = "<html><body>And a <i>very small</i> man " +
                    "can cast a <b>very large</b> shadow. </body></html>";
            webViewFragment.getWebView().loadData(summary, "text/html", null);

        });

    }

    @Override
    public void setOnSubmitListener(String arg) {
        mTextView.setText(arg);
    }

}
